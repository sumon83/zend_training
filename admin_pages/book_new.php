<?php
include('lib/dal/book_dal.php');

$book = new book();


$etitle = "";
$eisbn = "";
$etype = "";
$eauthorId = "";
$eimage = "";
$ebrochure = "";

if(isset($_POST['submit']))
{
    $book->title = $_POST['title'];
    $book->isbn = $_POST['isbn'];
    $book->type =  isset($_POST['type'])?$_POST["type"]:"";
    $book->authorId =  $_POST['author'];
    

    $er = 0;

    if($book->title == "")
    {
        $er++;
        $etitle = "Required";
    }
    if($book->isbn == "")
    {
        $er++;
        $eisbn = "Required";
    }
    if($book->type == "")
    {
        $er++;
        $etype = "Required";
    }

    if($book->authorId == "")
    {
        $er++;
        $eauthorId = "Required";
    }

    
    if($er == 0)
    {        
        if($book->Insert())
        {
            print '<span class="success">Data Saved Successfully</span>';
            $book = new book();
        }
        else 
            {
                print '<span class="error">'.mysqli_error($con).'</span>';           
            }
    }
}

?>
<form method="post">

<label>Title</label><br>
<input type="text" name="title" value="<?php print $book->title; ?>"/>
<span class="fielderror"><?php print $etitle; ?></span> <br>

<label>ISBN</label><br>
<input type="text" name="isbn" value="<?php print $book->isbn; ?>"/>
<span class="fielderror"><?php print $eisbn; ?></span><br>

<label>Author</label><br>
<select name="author">
    <option value="0">Select</option>
    <?php
    $sql = "select id, name from author";
    $result = mysqli_query($con, $sql);
    while($row = mysqli_fetch_assoc($result))
    {
        print '<option value="'.$row["id"].'">'.$row["name"].'</option>';
    }
    ?>
</select>
<span class="fielderror"><?php print $eauthorId; ?></span><br>

<label>Type</label>
<input type="radio" name="type" value="Academic"/> Academic
<input type="radio" name="type" value="Novel"/> Novel
<span class="fielderror"><?php print $etype; ?></span><br>

<label>Image</label><br>
<input type="file" name="image" />
<span class="fielderror"><?php print $eimage; ?></span><br>

<label>Brochure</label><br>
<input type="file" name="brochure" />
<span class="fielderror"><?php print $ebrochure; ?></span><br>


<input type="submit" name="submit" value="Submit"/>
</form>
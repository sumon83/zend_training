
<a href="?a=book_new">New Entry</a><br><hr><br>
<?php
$page = 1;
$numberofitem = 1;

if(isset($_GET['page']))
{
    $page = $_GET['page'];
}


$search = "";
$type = "";
if(isset($_POST['submit']))
{
    $search = $_POST['search'];
    $type = isset($_POST['type'])?$_POST['type']:"";
}

?>
<form method="post">
<input type="text" name="search" value="<?php print $search; ?>" />
<input type="submit" name="submit" value="Search"/>

</form>

<?php


$sql = "select id, title, isbn, type, author, image, brochure from book where id > 0";
if($type != "")
{
    $sql .= " and type='".ms($type)."'";
}
if($search != "")
{
    $sql .= " and (title like '%".ms($search)."%' or isbn like '%".ms($search)."%')";
}

$sql .= " limit ".(($page-1) * $numberofitem).", ".$numberofitem;

$result = mysqli_query($con, $sql);
print_r($result);

print '<table>';
print '<tr><th>Title</th><th>ISBN</th><th>Type</th><th>Author</th><th>Image</th><th>Brochure</th><th></th></tr>';
while($row = mysqli_fetch_assoc($result))
{
    print '<tr>';
    print '<td>'. str_replace($search, "<hl>".$search."</hl>", $row["title"]).'</td>';
    print '<td>'.$row["isbn"].'</td>';
    print '<td>'.$row["type"].'</td>';
    print '<td>'.$row["author"].'</td>';
    print '<td><img src="upload/book_image/'.$row["id"].'_'.$row["image"].'" /></td>';
    print '<td><a href="upload/book_brochure/'.$row["id"].'_'.$row["brochure"].'">Download</a></td>';
    print '<td><a href="#">Edit</a> | <a href="#">Delete</a></td>';
    print '</tr>';
}
print '</table>';

?>

<a href="?a=book&page=1">Page 1</a>
<a href="?a=book&page=2">Page 2</a>
<a href="?a=book&page=3">Page 3</a>
